/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Acer
 */
public class TestShape {
    public static void main(String[] args) {
        Shape shape=new Shape();
        shape.calArea();
        shape.print();
        System.out.println("-------");
        
        Circle circle1 =new Circle(3);
        circle1.calArea();
        circle1.print();
        System.out.println("-------");
        
        Circle circle2 =new Circle(4);
        circle2.calArea();
        circle2.print();
        System.out.println("-------");
        
        Triangle triangle1= new Triangle(3,4);
        triangle1.calArea();
        triangle1.print();
        System.out.println("-------");
        
        Rectangle rectangle1= new Rectangle(3,4);
        rectangle1.calArea();
        rectangle1.print();
        System.out.println("-------");
        
        Square square1 = new Square(2);
        square1.calArea();
        square1.print();
        System.out.println("-------");
    }
        
}
