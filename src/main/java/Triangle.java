/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Acer
 */
public class Triangle {
    public double base;
    public double height;
    public static final double half = 0.5;
    public Triangle(double height,double base){
        this.height =height;
        this.base =base;
        System.out.println("Triangle created");
    }
    public double calArea(){
        return half*height*base;
    }
    public void print(){
        System.out.println("Area: "+calArea());
    }
    
}
